# ValutaChange Take Home Assignments

This repository contains several take home assignments given to candidates applying for an engineering role at [ValutaChange](https://valutachange.de). More information on each assignment can be found in the respective subdirectory.
